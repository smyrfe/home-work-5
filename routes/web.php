<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', function () {
    $member = new App\Member();

    if($member->check()){
        return redirect()->route('profile');
    }
    return view('welcome', ['member' => $member]);
});

Route::get('/login', 'CustomAuthController@getLogin');
Route::get('/logout', 'CustomAuthController@logout');

Route::get('/registration', 'Register@getRegister');
Route::get('/image', function (){
    return view('pages.upload_image');
});
Route::get('/profile/edit', ['as' => 'profile_edit', 'uses' => 'ProfileController@getEdit']);
Route::get('/profile', ['as' => 'profile', 'uses' => 'ProfileController@index']);

Route::get('/profiles', ['middleware' => 'check-roles:administrator', 'uses' => 'ProfileController@showListAccounts']);
Route::get('/profiles/{id}/edit', ['middleware' => 'check-roles:administrator','as' => 'edit.profile', 'uses' => 'ProfileController@editAnyProfile'])->where('id', '[0-9]+');
Route::get('/profiles/{id}', ['middleware' => 'check-roles:administrator','as' => 'profiles', 'uses' => 'ProfileController@show'])->where('id', '[0-9]+');


Route::post('/registration', 'Register@postRegister');
Route::post('/login', 'CustomAuthController@postLogin');
Route::post('/profile/edit', 'ProfileController@postEdit');


