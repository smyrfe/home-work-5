<?php

namespace App\Http\Middleware;
use App\Member;
use Closure;
use GuzzleHttp\Psr7\Response;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {

        $member = new Member();

        if (!$member->check()){
            return redirect('/login');
        }

        $member = $member->currentMember();

        if (!$member->hasRole($role)) {
            return \response(view('forbidden'));
        }
        return $next($request);
    }
}
