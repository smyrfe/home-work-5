<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;


class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    protected $rules = [
        'first_name' => 'required|min:2|regex: /[\x{0430}-\x{044F}]/u',
        'last_name' => 'required|min:2|regex: /[\x{0430}-\x{044F}]/u',
        'sex' => 'required',
        'email' => 'required|email|unique:members,email',
        'account_name' => 'required|min:2|unique:members,account_name',
        'password' => 'required|min:8',
        'phone_number' => 'required|regex: /[0-9]{11}/u',
        'avatar' => 'image'
    ];

    public function rules()
    {
        return $this->rules;
    }

    public $messages = array(
        'required' => 'Поле :attribute должно быть заполнено',
        'regex' => 'Поле :attribute заполнено в некорректном формате',
        'min' => 'Поле :attribute должно содержать минимум :min символов'
    );

    public function messages()
    {
        return $this->messages;
    }


}
