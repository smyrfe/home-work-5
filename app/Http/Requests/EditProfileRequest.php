<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected $rules = [
        'first_name' => 'required|min:2|regex: /[\x{0430}-\x{044F}]/u',
        'last_name' => 'required|min:2|regex: /[\x{0430}-\x{044F}]/u',
        'sex' => 'required',
        'password' => 'required|min:8',
        'phone_number' => 'required|regex: /[0-9]{11}/u',
        'avatar' => 'image'
    ];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->rules;
    }

    public $messages = array(
        'required' => 'Поле :attribute должно быть заполнено',
        'regex' => 'Поле :attribute заполнено в некорректном формате',
        'min' => 'Поле :attribute должно содержать минимум :min символов'
    );

    public function messages()
    {
        return $this->messages;
    }
}
