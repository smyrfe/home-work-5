<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'account_name' => 'required',
            'password' => 'required'
            //
        ];
    }

    public $messages = array(
        'required' => 'Поле :attribute должно быть заполнено',
        'regex' => 'Поле :attribute заполнено в некорректном формате',
        'min' => 'Поле :attribute должно содержать минимум :min символов'
    );

    public function messages()
    {
        return $this->messages;
    }
}
