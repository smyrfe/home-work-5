<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditProfileRequest;
use Illuminate\Http\Request;
use App\Member;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
class ProfileController extends Controller
{
    // Страничка авторизированного пользователя
    public function index()
    {
        if (!is_null(Session::get('user')))
        {
            $member = Member::where('account_name', Session::get('user'))->first();

            return view('profile.index', ['member' => $member]);
        }
        else {
            return redirect('login')->withErrors('Вы не вошли в систему');
        }

    }
    //Показ чужого профиля
    public function show($id)
    {
        if (!is_null(Session::get('user')))
        {
            if(!is_null($member = Member::find($id))) {

                return view('profile.show', ['member' => $member]);

            } else {
                return view('404');
            }

        } else {
            return redirect('login')->withErrors('Вы не вошли в систему');
        }
    }

    public  function getEdit()
    {
        if (!is_null(Session::get('user')))
        {
            $member = Member::where('account_name', '=', Session::get('user'))->first()->toArray();

            return view('profile.edit', ['member' => $member]);
        }
    }

    public function postEdit(EditProfileRequest $request)
    {
        $data = $request->all();

        if($request->hasFile('avatar')) {

            $input_file = $request->file('avatar');

            $file_name = $request->account_name . time() . '.' . $input_file->getClientOriginalExtension();

            $input_file->move(public_path('\avatars\\'), $file_name);

            $data['avatar'] = $file_name;

        } else {
            $data['avatar'] = 'default.jpg';
        }

        $member = new Member();

        $member = $member->updateInfo($data);

        return redirect()->route('profile')->with(['member' => $member]);

    }
    //Список зарег. пользователей
    public function showListAccounts()
    {
        $accounts = Member::all();

        return view('profile.list')->with('accounts', $accounts);

    }
    //Редактирование чужого профиля
    public function editAnyProfile($id)
    {
        $member = Member::find($id)->toArray();

        if(!is_null($member))
        {
            return view('profile.edit')->with('member', $member);
        } else {
            return abort(404);
        }

    }


}
