<?php

namespace App\Http\Controllers;

use App\Http\Controllers;
use Illuminate\Http\Request;
use App\Member;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\RegisterRequest;


class Register extends Controller
{

    public function getRegister()
    {
        return view ('pages.registration');
    }


    public function setInfo(Request $request)
    {

    }


    public function postRegister(RegisterRequest $request)
    {

       $member = new Member($request->toArray());

       $member->password = bcrypt($request->password);

       if($request->hasFile('avatar')){

           $input_file = $request->file('avatar');

           $file_name = $request->account_name . "." .$input_file->getClientOriginalExtension();

           $input_file->move(public_path('\avatars\\'), $file_name);

           $member->avatar = $file_name;

       }

       $member->save();

       return redirect('/login');
    }
}
