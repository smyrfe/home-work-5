<?php

namespace App\Http\Controllers;
use App\Member;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;



class CustomAuthController extends Controller
{
    public function getLogin()
    {
        if (!is_null(Session::get('user')))
        {
            return redirect('/profile');
        }
        return view('pages.login');
    }

    public function postLogin(LoginRequest $request)
    {
       $account_name = $request->account_name;
       $password = $request->password;

       $member = new Member();

        if ($member->verifyAuth($account_name, $password)){
            return redirect()->route('profile');
        } else {
            return Redirect::back()->withErrors('Логин/E-mail или пароль неверны');
        }

    }

    public function logout()
    {
        Session::forget('user');

        return redirect('/');

    }
}
