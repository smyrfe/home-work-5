<?php

namespace App;

use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class Member extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'sex',
        'email',
        'account_name',
        'phone_number',
        'avatar',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role')->withTimestamps();
    }

    
    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()){
            return true;
        };

        return false;
    }
    
    public function verifyAuth($account_name, $password)
    {
        $member = new Member();

        $member = Member::where('account_name', '=', $account_name)->orWhere('email', '=', $account_name)->first();

        if(!is_null($member)){

            $member->toArray();

            if (password_verify($password, $member['password'])) {

                Session::put('user', $member['account_name']);

                return true;

            } else {

                return false;
            }
        }
            return false;
    }
    // Проверка на авторизацию
    public function check()
    {
        if (!is_null(Session::get('user')))
        {
            return true;
        }
        else {
            return false;
        }
    }
    //Возвращает текущего участника
    public function currentMember()
    {
        if ($this->check())
        {
            $member = Session::get('user');

            $info = Member::where('account_name', '=', $member)->first();

            return $info;

        }
    }
    //Обновление инфы после редактирования
    public function updateInfo($data)
    {
        $member = Member::where('account_name', '=', $data['account_name'])->first();

        $member->update([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'sex' => $data['sex'],
                'password' => bcrypt($data['password']),
                'phone_number' => $data['phone_number'],
                'avatar' => $data['avatar']
            ]
        );


    }



}
