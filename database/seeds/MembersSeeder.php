<?php

use Illuminate\Database\Seeder;
use App\Member;
use App\Role;

class MembersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role_administrator = Role::where('name', 'administrator')->first();
        $role_member = Role::where('name', 'member')->first();

        $admin = new Member();

        $admin->first_name = 'Иван';
        $admin->last_name = 'Иванов';
        $admin->sex = 'Мужской';
        $admin->email = 'admin@mysite.ru';
        $admin->account_name = 'ivanov';
        $admin->password = bcrypt('123');
        $admin->phone_number = '12345678910';
        $admin->save();

        $admin->roles()->attach($role_administrator);

        $member = new Member();

        $member->first_name = 'Юрий';
        $member->last_name = 'Иванов';
        $member->sex = 'Мужской';
        $member->email = 'member@mysite.ru';
        $member->account_name = 'simple';
        $member->password = bcrypt('123');
        $member->phone_number = '10987654321';
        $member->save();

        $member->roles()->attach($role_member);
    }
}
