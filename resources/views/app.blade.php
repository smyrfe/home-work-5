<!doctype html>
<html>
<head>
        <meta charset="UTF-8">
        <title>Document</title>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <style>
        h1 {
            font:  300% italic;
        }

        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>

     <div class="container">
         <div class="top-right links">
             @if (Session::get('user'))
                 <a href="{{ url('/profile') }}">Profile</a>
                 <a href="{{ url('/logout') }}">Logout</a>
             @elseif ($_SERVER['REQUEST_URI'] == "/login")
                 <a href="{{ url('/registration') }}">Registration</a>
             @elseif (($_SERVER['REQUEST_URI'] == "/registration"))
                 <a href="{{ url('/login') }}">Login</a>
             @endif
         </div>
         @yield('content')
     </div>
</body>
</html>