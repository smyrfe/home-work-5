@extends('app')

@section('content')

    <h1>Список всех пользователей</h1>
    <hr/>

    @foreach($accounts as $account)
        <ul>
            <li><h4/><a href="/profiles/{{$account->id}}">{{strtoupper($account->account_name)}}</a></li>
        </ul>

    @endforeach

@stop