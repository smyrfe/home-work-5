@extends ('app')

@section('content')
    <style>
        .custom-width{
            width: 40%;
            margin-left: 350px;
        }
        h1 {
            text-align: left;
        }

    </style>

    <h1> {{ strtoupper($member['account_name']) }}</h1>
    <hr/>

    <div class="custom-width">
        <img src="{{ asset('avatars/' . $member['avatar'])  }}" alt="profile Pic" class="avatar"  height="250">
    {!! Form::open(['files' => true, 'action' => 'ProfileController@postEdit']) !!}
    <div class="form-group">

        <div class="form-group">
            {!! Form::label('first_name', 'Имя') !!}
            {!! Form::text('first_name', $member['first_name'], ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('last_name', 'Фамилия') !!}
            {!! Form::text('last_name', $member['last_name'], ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('sex', 'Пол') !!}
            {!! Form::select('sex', ['Мужской' => 'Мужской', 'Женский' => 'Женский'], ['placeholder' => $member['sex']], ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('password', 'Пароль') !!}
            {!! Form::password('password', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('phone_number', 'Номер телефона') !!}
            {!! Form::text('phone_number', $member['phone_number'], ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('Зазрузите аватар') !!}
            {!! Form::file('avatar') !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Изменить профиль', ['class' => 'brn btn-primary form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::hidden('account_name', $member['account_name']) !!}
        </div>

    </div>
    </div>
    {!! Form::close() !!}

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>

            @endforeach
        </ul>
    @endif
@stop