@extends('app')

@section('content')

    <h1> {{strtoupper($member['account_name']) }}</h1>
    <hr/>
    <img src="{{ asset('avatars/' . $member['avatar'])  }}" alt="profile Pic" class="pull-right" height="250">

    <h3>Личная информация</h3>
    <ul>
        <li>Имя: {{$member['first_name']}}</li>
        <li>Фамилия: {{$member['last_name']}}</li>
        <li>Пол: {{$member['sex']}}</li>
        <li>Имя в системе: {{$member['account_name']}}</li>
        <li>E-mail: {{$member['email']}}</li>
        <li>Номер телефона: {{$member['phone_number']}}</li>
        <li>Дата регистрации: {{$member['created_at']}}</li>
    </ul>

    <a href="{{ route('edit.profile', ['id' => $member['id']]) }}" class="btn btn-large btn-primary ">Изменить профиль</a>
@stop



