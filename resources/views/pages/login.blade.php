@extends('app')
<style>
    .wrapper {
        margin-top: 80px;
        margin-bottom: 80px;
    }
    .form-signin {
        max-width: 380px;
        padding: 15px 35px 45px;
        margin: 0 auto;
        background-color: #fff;
        border: 1px solid rgba(0, 0, 0, 0.1);

    }
    .form-signin-heading,
    .checkbox {
        margin-bottom: 30px;
    }

    .checkbox {
        font-weight: normal;
    }

    .form-control {
        position: relative;
        font-size: 16px;
        height: auto;
        padding: 10px;
    }

    input[type="text"] {
        margin-bottom: -1px;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
    }

    input[type="password"] {
        margin-bottom: 20px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
    }
    }

</style>
@section('content')
    <div class="wrapper">
        {!! Form::open(['method' => 'post','class' => 'form-signin']) !!}

        <h2 class="form-signin-heading">Войдите в аккаунт</h2>
        {!! Form::text('account_name', null, ['class' => 'form-control', 'placeholder' => 'Логин или E-mail']) !!}

        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Пароль']) !!}


        {!! Form::submit('Войти', ['class' => 'btn btn-lg btn-primary btn-block']) !!}

        {!! Form::close() !!}
    </div>

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>

            @endforeach
        </ul>
    @endif
@stop