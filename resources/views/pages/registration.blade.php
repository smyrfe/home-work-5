@extends('app')

<style>
    .custom-width{
        width: 50%;
    }


</style>
@section('content')
    <h1>Регистрация нового участника</h1>
    <hr/>
    <div class="custom-width">
        {!! Form::open(['files' => true]) !!}
        <div class="form-group">
            <div class="form-group">
                {!! Form::label('first_name', 'Имя') !!}
                {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('last_name', 'Фамилия') !!}
                {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('sex', 'Пол') !!}
                {!! Form::select('sex', ['Мужской' => 'Мужской', 'Женский' => 'Женский'], ['placeholder' => 'Выберите пол'], ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('email', 'E-Mail') !!}
                {!! Form::text('email', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('account_name', 'Имя аккаунта') !!}
                {!! Form::text('account_name', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('password', 'Пароль') !!}
                {!! Form::password('password', ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('phone_number', 'Номер телефона') !!}
                {!! Form::text('phone_number', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('Зазрузите аватар') !!}
                {!! Form::file('avatar') !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Зарегистрироваться', ['class' => 'brn btn-primary form-control']) !!}
            </div>

    </div>

    {!! Form::close() !!}
    </div>

    @if ($errors->any())
        <ul class="alert alert-danger">
          @foreach ($errors->all() as $error)
            <li>{{  $error }}</li>
            @endforeach
        </ul>
    @endif


@stop